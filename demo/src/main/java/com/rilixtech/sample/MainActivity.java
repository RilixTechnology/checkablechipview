package com.rilixtech.sample;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;
import com.rilixtech.widget.CheckableChipView;

public class MainActivity extends AppCompatActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    CheckableChipView cp1 = findViewById(R.id.chip1);
    CheckableChipView cp2 = findViewById(R.id.chip2);

    CheckableChipView.OnCheckedChangeListener listener = new CheckableChipView.OnCheckedChangeListener() {
      @Override public void onCheckedChanged(CheckableChipView view, boolean isChecked) {
        Toast.makeText(MainActivity.this, "view: " + view.getId() + ", isChecked = " + isChecked, Toast.LENGTH_SHORT).show();
      }
    };
    cp1.setOnCheckedChangeListener(listener);
    cp2.setOnCheckedChangeListener(listener);
  }
}
