package com.rilixtech.widget;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.core.graphics.ColorUtils;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.animation.PathInterpolatorCompat;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SoundEffectConstants;
import android.view.View;
import android.widget.Checkable;
import android.widget.TextView;

/**
 * A custom view for displaying filters. Allows a custom presentation of the tag color and selection
 * state.
 */
public class CheckableChipView extends View implements Checkable {
  private static final String TAG = CheckableChipView.class.getSimpleName();
  private static final long CHECKING_DURATION = 350L; // ms
  private static final long UNCHECKING_DURATION = 200L; // ms

  /**
   * Sets the indicator and background color when the widget is checked.
   */
  private int checkedColor = 0;

  public void setCheckedColor(int checkedColor) {
    if (this.checkedColor == checkedColor) return;

    this.checkedColor = checkedColor;
    indicatorPaint.setColor(checkedColor);
    ViewCompat.postInvalidateOnAnimation(this);
  }

  /**
   * Sets the text color to be used when the widget is not checked.
   */
  private int defaultTextColor = 0;

  public void setDefaultTextColor(int defaultTextColor) {
    if (this.defaultTextColor == defaultTextColor) return;

    this.defaultTextColor = defaultTextColor;
    textPaint.setColor(defaultTextColor);
    ViewCompat.postInvalidateOnAnimation(this);
  }

  /**
   * Sets the text color to be used when the widget is checked.
   */
  private int checkedTextColor = 0;

  public void setCheckedTextColor(int checkedTextColor) {
    if (this.checkedTextColor == checkedTextColor) return;

    this.checkedTextColor = checkedTextColor;
    ViewCompat.postInvalidateOnAnimation(this);
  }

  /**
   * Sets the text to be displayed.
   */
  private CharSequence text = "";

  public void setText(CharSequence text) {
    if (text == null) return;
    if (this.text.equals(text)) return;

    this.text = text;
    requestLayout();
  }

  /**
   * Sets the textSize to be displayed.
   */
  private Float textSize = 0f;

  public void setTextSize(Float textSize) {
    if (textSize == null) return;

    if (this.textSize == null) {
      this.textSize = textSize;
    } else {
      if (this.textSize.equals(textSize)) return;
    }
    this.textSize = textSize;
    textPaint.setTextSize(textSize);
    requestLayout();
  }

  /**
   * Controls the color of the outline.
   */
  private int outlineColor = 0;

  public void setOutlineColor(int outlineColor) {
    if (this.outlineColor == outlineColor) return;

    this.outlineColor = outlineColor;
    outlinePaint.setColor(outlineColor);

    ViewCompat.postInvalidateOnAnimation(this);
  }

  /**
   * Controls the stroke width of the outline.
   */
  private Float outlineWidth = 0f;

  public void setOutlineWidth(Float outlineWidth) {
    if (outlineWidth == null) return;

    if (this.outlineWidth == null) {
      this.outlineWidth = outlineWidth;
    } else {
      if (this.outlineWidth.equals(outlineWidth)) return;
    }

    this.outlineWidth = outlineWidth;
    outlinePaint.setStrokeWidth(outlineWidth);
    ViewCompat.postInvalidateOnAnimation(this);
  }

  /**
   * Controls the corner radius of the outline. If null the outline will be pill-shaped.
   */
  private Float outlineCornerRadius = null;

  public void setOutlineCornerRadius(Float outlineCornerRadius) {
    if (this.outlineCornerRadius == null) {
      this.outlineCornerRadius = outlineCornerRadius;
      ViewCompat.postInvalidateOnAnimation(this);
    } else {
      if (this.outlineCornerRadius.equals(outlineCornerRadius)) return;
      this.outlineCornerRadius = outlineCornerRadius;
      ViewCompat.postInvalidateOnAnimation(this);
    }
  }

  //    /**
  // * Sets the listener to be called when the checked state changes.
  // */
  //var listener: ((view: CheckableChipView, checked: Boolean) -> Unit)? = null

  private Float targetProgress = 0f;

  public interface OnCheckedChangeListener {
    void onCheckedChanged(CheckableChipView view, boolean isChecked);
  }

  private OnCheckedChangeListener listener;

  public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
    this.listener = listener;
  }

  private Float progress = 0f;

  private void setProgress(Float progress) {
    if (progress == null) return;

    if (this.progress == null) {
      this.progress = progress;
    } else {
      if (this.progress.equals(progress)) return;
      this.progress = progress;
    }
    if (progress == 0f || progress == 1f) {
        if(listener != null) listener.onCheckedChanged(this, isChecked());
      }
    ViewCompat.postInvalidateOnAnimation(this);
  }

  private Integer padding = 0;
  private Paint outlinePaint;
  private TextPaint textPaint;
  private Paint indicatorPaint;

  private Drawable clearDrawable;
  private Drawable touchFeedbackDrawable;
  private StaticLayout textLayout;

  private ValueAnimator progressAnimator;

  private ValueAnimator initProgressAnimator() {
    ValueAnimator animator = ValueAnimator.ofFloat();
    //TimeInterpolator interpolator = AnimationUtils.loadInterpolator(getContext(),
    //    android.R.interpolator.fast_out_slow_in);
    TimeInterpolator interpolator = PathInterpolatorCompat.create(0.4F, 0F, 0.2F,1F);
    animator.setInterpolator(interpolator);
    return animator;
  }

  public CheckableChipView(Context context) {
    super(context);
    init(null);
  }

  public CheckableChipView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init(attrs);
  }

  public CheckableChipView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init(attrs);
  }

  private void init(AttributeSet attrs) {
    Context context = getContext();
    TypedArray arr = context.obtainStyledAttributes(attrs,
        R.styleable.CheckableChipView,
        R.attr.checkableChipViewStyle,
        R.style.Widget_CheckableChipView);

    outlinePaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    indicatorPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
    outlinePaint.setStyle(Paint.Style.STROKE);

    outlineColor = arr.getColor(R.styleable.CheckableChipView_ccv_outlineColor, 0);
    outlinePaint.setColor(outlineColor);
    //setOutlineColor(tempOutlineColor);

    outlineWidth = arr.getDimension(R.styleable.CheckableChipView_ccv_outlineWidth, 0);
    outlinePaint.setStrokeWidth(outlineWidth);
    //setOutlineWidth(tempOutlineWidth);

    if (arr.hasValue(R.styleable.CheckableChipView_ccv_outlineCornerRadius)) {
      outlineCornerRadius =
          arr.getDimension(R.styleable.CheckableChipView_ccv_outlineCornerRadius, 0);
      //setOutlineCornerRadius(tempOutlineCornerRadius);
    }

    checkedColor = arr.getColor(R.styleable.CheckableChipView_android_color, checkedColor);
    indicatorPaint.setColor(checkedColor);
    //setCheckedColor(tempCheckedColor);

    checkedTextColor =
        arr.getColor(R.styleable.CheckableChipView_ccv_checkedTextColor, Color.TRANSPARENT);
    //setCheckedTextColor(tempCheckedTextColor);

    defaultTextColor = arr.getColor(R.styleable.CheckableChipView_android_textColor, 0);
    textPaint.setColor(defaultTextColor);
    //setDefaultTextColor(tempDefaultTextColor);

    text = arr.getString(R.styleable.CheckableChipView_android_text);

    // TODO: What is this for???
    TextView textView = new TextView(getContext());
    //textView.setText(tempText);

    textSize = arr.getDimension(R.styleable.CheckableChipView_android_textSize,
        textView.getTextSize());
    textPaint.setTextSize(textSize);
    //setTextSize(tempTextSize);

    clearDrawable = arr.getDrawable(R.styleable.CheckableChipView_ccv_clearIcon);
    if (clearDrawable != null) {
      int intrinsicWidth = clearDrawable.getIntrinsicWidth();
      int intrinsicHeight = clearDrawable.getIntrinsicHeight();
      clearDrawable.setBounds(-intrinsicWidth / 2,
          -intrinsicHeight / 2,
          intrinsicWidth / 2,
          intrinsicHeight / 2);
    }

    touchFeedbackDrawable = arr.getDrawable(R.styleable.CheckableChipView_ccv_foreground);
    if (touchFeedbackDrawable != null) { touchFeedbackDrawable.setCallback(this); }
    padding = arr.getDimensionPixelSize(R.styleable.CheckableChipView_android_padding, 0);

    isChecked = arr.getBoolean(R.styleable.CheckableChipView_android_checked, false);
    targetProgress = isChecked ? 1.0F : 0F;
    //setChecked(isChecked);
    //setSaveEnabled(true);

    //setText(text);
    arr.recycle();

    //AMViewCompat.setClipToOutline(this, true);
    setClickable(true);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    int widthMode = MeasureSpec.getMode(widthMeasureSpec);
    int heightMode = MeasureSpec.getMode(heightMeasureSpec);

    // width
    int nonTextWidth = (4 * padding)
        + (int) (2 * outlinePaint.getStrokeWidth())
        + clearDrawable.getIntrinsicWidth();

    int availableTextWidth;
    switch (widthMode) {
      case MeasureSpec.EXACTLY:
      case MeasureSpec.AT_MOST:
        availableTextWidth = MeasureSpec.getSize(widthMeasureSpec) - nonTextWidth;
        break;
      case MeasureSpec.UNSPECIFIED:
      default:
        availableTextWidth = Integer.MAX_VALUE;
    }

    createLayout(availableTextWidth);
    int desiredWidth = nonTextWidth + CcvUtils.textWidth(textLayout);//textLayout.textWidth()
    final int width;
    switch (widthMode) {
      case MeasureSpec.EXACTLY:
        width = MeasureSpec.getSize(widthMeasureSpec);
        break;
      case MeasureSpec.AT_MOST:
        width = Math.min(MeasureSpec.getSize(widthMeasureSpec), desiredWidth);
        break;
      case MeasureSpec.UNSPECIFIED:
      default:
        width = desiredWidth;
    }

    // height
    int desiredHeight = padding + textLayout.getHeight() + padding;
    final int height;
    switch (heightMode) {
      case MeasureSpec.EXACTLY:
        height = MeasureSpec.getSize(heightMeasureSpec);
        break;
      case MeasureSpec.AT_MOST:
        height = Math.min(MeasureSpec.getSize(heightMeasureSpec), desiredHeight);
        break;
      case MeasureSpec.UNSPECIFIED:
      default:
        height = desiredHeight;
    }

    setMeasuredDimension(width, height);

    final Float radius;
    if (outlineCornerRadius == null) {
      radius = (float) height / 2f;
    } else {
      radius = outlineCornerRadius;
    }

    //setOutlineProvider(new ViewOutlineProvider() {
    //  @Override public void getOutline(View view, Outline outline) {
    //    outline.setRoundRect(0, 0, width, height, radius);
    //  }
    //});
    //if (clipOutlineProvider == null) clipOutlineProvider = new MyOutlineProvider(width, height);
    //AMViewCompat.setOutlineProvider(this, clipOutlineProvider);
    setMyOutline(width, height, radius);

    touchFeedbackDrawable.setBounds(0, 0, width, height);
  }

  //private void invalidateClipOutline(boolean redraw) {
  //  if (mProvider == null) {
  //    return;
  //  }
  //  mClipPath.reset();
  //  mClipPath.addRect(-1, -1, getWidth() + 1, getHeight() + 1,
  //      Path.Direction.CW);
  //  mOutlinePath.reset();
  //  mProvider.getOutline(this, mOutlinePath);
  //  mClipPath.addPath(mOutlinePath);
  //  if (redraw) {
  //    invalidate();
  //  }
  //}
  //
  //@Override
  //protected void onSizeChanged(int w, int h, int oldw, int oldh) {
  //  super.onSizeChanged(w, h, oldw, oldh);
  //  invalidateClipOutline(false);
  //}

  private final Path mClipPath = new Path();
  //private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
  //private final Xfermode mXfermode = new PorterDuffXfermode(PorterDuff.Mode.CLEAR);
  private final Path mOutlinePath = new Path();
  private ClipOutlineProvider mProvider;

  private void setMyOutline(int width, int height, float radius) {
    if (mProvider != null) return;

    mClipPath.setFillType(Path.FillType.EVEN_ODD);
    mOutlinePath.setFillType(Path.FillType.EVEN_ODD);
    mProvider = new RoundRectClipOutlineProvider(width, height, radius);
    //mProvider = new RoundRectClipOutlineProvider(radius);
    //final ClipOutlineProvider provider = ConstructorHelper.newInstance(getContext(), "circle",
    //        isInEditMode(), this, ClipOutlineProvider.class,
    //        attrs, R.styleable.CheckableChipView,
    //R.attr.checkableChipViewStyle);

    //if (provider == null)
    //    mProvider = provider;
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    outlinePaint.setStrokeWidth(outlineWidth);
    outlinePaint.setColor(outlineColor);

    Float iconRadius = clearDrawable.getIntrinsicWidth() / 2.0F;
    Float halfStroke = outlineWidth / 2.0F;

    Float rounding;
    if (outlineCornerRadius == null) {
      rounding = ((float) getHeight() - outlineWidth) / 2.0F;
    } else {
      rounding = outlineCornerRadius;
    }

    // Outline
    if (progress.compareTo(1.0F) < 0) {
      canvasDrawRoundRect(canvas,
          halfStroke,
          halfStroke,
          ((float) getWidth()) - halfStroke,
          ((float) getHeight()) - halfStroke,
          rounding,
          rounding,
          outlinePaint
      );
    }

    // Draws beyond bounds and relies on clipToOutline to enforce shape
    Float initialIndicatorSize = (float) clearDrawable.getIntrinsicWidth();
    Float indicatorCenterX = outlineWidth
        + (float) padding
        + ((float) padding / 2.0F)
        + (initialIndicatorSize / 2.0F);

    Float indicatorCenterY = (float) getHeight() / 2.0F;

    Float indicatorSize = CcvUtils.lerp(
        initialIndicatorSize,
        Math.max(((float) getWidth() - indicatorCenterX) * 2.0F,
            ((float) getHeight() - indicatorCenterY) * 2.0F),
        progress
    );

    Float indicatorSizeHalf = indicatorSize / 2.0F;

    Float indicatorRounding =
        (rounding / ((float) getHeight() - outlineWidth)) * (indicatorSizeHalf * 2f);
    indicatorPaint.setColor(checkedColor);

    canvasDrawRoundRect(canvas,
        indicatorCenterX - indicatorSizeHalf,
        indicatorCenterY - indicatorSizeHalf,
        indicatorCenterX + indicatorSizeHalf,
        indicatorCenterY + indicatorSizeHalf,
        indicatorRounding,
        indicatorRounding,
        indicatorPaint
    );

    // Text
    Float textX = CcvUtils.lerp(
        indicatorCenterX + initialIndicatorSize / 2f + (float) padding,
        outlineWidth + (float) padding + (float) padding / 2f,
        progress
    );

    int color;
    if (checkedTextColor == 0) {
      color = defaultTextColor;
    } else {
      color = ColorUtils.blendARGB(defaultTextColor, checkedTextColor, progress);
    }
    textPaint.setTextSize(textSize);
    textPaint.setColor(color);

    float x = textX;
    float y = (getHeight() - textLayout.getHeight()) / 2f;
    canvas.translate(x, y);
    textLayout.draw(canvas);

    // Clear icon
    if (progress.compareTo(0f) > 0) {
      //TODO: 2 * (float) padding) is only a hack, need to remove it and check the previous code.
      float dx = (float) getWidth() - outlineWidth - 2 * (float) padding - iconRadius;
      float dy = ((float) getHeight() - 2 * (float) padding) / 2.0F;

      canvas.translate(dx, dy);
      canvas.scale(progress, progress);
      clearDrawable.draw(canvas);
    }

    // Touch feedback
    touchFeedbackDrawable.draw(canvas);
  }

  private void canvasDrawRoundRect(Canvas canvas, float left, float top, float right, float bottom,
      float rx, float ry, Paint paint) {
    RectF rectf = new RectF(left, top, right, bottom);
    canvas.drawRoundRect(rectf, rx, ry, paint);
  }

  /**
   * Starts the animation to enable/disable a filter and invokes a function when done.
   */
  private void setCheckedAnimated(boolean checked) {
    targetProgress = checked ? 1.0F : 0F;
    if (progress.equals(targetProgress)) return;
    Log.d(TAG, "targetProgress = " + targetProgress);

    if (progressAnimator == null) progressAnimator = initProgressAnimator();
    progressAnimator.removeAllListeners();
    progressAnimator.cancel();
    progressAnimator.setFloatValues(progress, targetProgress);
    progressAnimator.setDuration(checked ? CHECKING_DURATION : UNCHECKING_DURATION);
    progressAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
      @Override public void onAnimationUpdate(ValueAnimator animation) {
        Float tempProgress = (Float) animation.getAnimatedValue();
        setProgress(tempProgress);
      }
    });

    progressAnimator.addListener(new Animator.AnimatorListener() {
      @Override public void onAnimationStart(Animator animation) {

      }

      @Override public void onAnimationEnd(Animator animation) {
        setProgress(targetProgress);
      }

      @Override public void onAnimationCancel(Animator animation) {

      }

      @Override public void onAnimationRepeat(Animator animation) {

      }
    });

    progressAnimator.start();
  }

  @Override
  public boolean performClick() {
    setCheckedAnimated(!isChecked());
    boolean handled = super.performClick();
    if (!handled) {
      playSoundEffect(SoundEffectConstants.CLICK);
    }
    return handled;
  }

  @Override public boolean isChecked() {
    Log.d(TAG, "targetProgress: " + targetProgress);
    isChecked = targetProgress.compareTo(1.0F) == 0;
    Log.d(TAG, "isChecked inside isChecked() = " + isChecked);
    return isChecked;
  }

  @Override public void toggle() {
    setChecked(!isChecked());
  }

  private boolean isChecked;

  @Override public void setChecked(boolean checked) {
    targetProgress = checked ? 1.0F : 0F;
    setProgress(targetProgress);
    isChecked = checked;
  }

  private void createLayout(int textWidth) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      textLayout = StaticLayout.Builder.obtain(text, 0,
          text.length(), textPaint, textWidth)
          .build();
    } else {
      textLayout =
          new StaticLayout(text, textPaint, textWidth, Layout.Alignment.ALIGN_NORMAL, 1f, 0f, true);
    }
  }

  @Override
  protected boolean verifyDrawable(Drawable who) {
    return super.verifyDrawable(who) || who == touchFeedbackDrawable;
  }

  @Override
  protected void drawableStateChanged() {
    super.drawableStateChanged();
    touchFeedbackDrawable.setState(getDrawableState());
  }

  @Override
  public void jumpDrawablesToCurrentState() {
    super.jumpDrawablesToCurrentState();
    touchFeedbackDrawable.jumpToCurrentState();
  }

  @Override
  public void drawableHotspotChanged(float x, float y) {
    super.drawableHotspotChanged(x, y);
    DrawableCompat.setHotspot(touchFeedbackDrawable, x, y);
    //touchFeedbackDrawable.setHotspot(x, y);
  }

  @Override
  public Parcelable onSaveInstanceState() {
    Parcelable superState = super.onSaveInstanceState();
    SavedState ss = new SavedState(superState);
    ss.checked = isChecked();
    Log.d(TAG, "State checked :" + isChecked());
    return ss;
  }

  @Override
  public void onRestoreInstanceState(Parcelable state) {
    if (!(state instanceof SavedState)) {
      super.onRestoreInstanceState(state);
      return;
    }

    SavedState savedState = (SavedState) state;
    super.onRestoreInstanceState(savedState.getSuperState());

    OnCheckedChangeListener tempListener = listener;
    listener = null;
    setChecked(savedState.checked);
    listener = tempListener;
  }

  static class SavedState extends BaseSavedState {
    boolean checked;

    SavedState(Parcelable state) {
      super(state);
    }

    private SavedState(Parcel parcel) {
      super(parcel);
      checked = parcel.readInt() == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      super.writeToParcel(dest, flags);
      dest.writeInt(checked ? 1 : 0);
    }

    @Override
    public int describeContents() {
      return 0;
    }

    //required field that makes Parcelables from a Parcel
    public static final Parcelable.Creator<SavedState> CREATOR =
        new Parcelable.Creator<SavedState>() {
          public SavedState createFromParcel(Parcel source) {
            return new SavedState(source);
          }

          public SavedState[] newArray(int size) {
            return new SavedState[size];
          }
        };
  }
}
