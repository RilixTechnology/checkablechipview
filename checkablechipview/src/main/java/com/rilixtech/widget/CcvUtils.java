package com.rilixtech.widget;

import android.text.StaticLayout;


/**
 * Calculated the widest line in a [StaticLayout].
 */
public class CcvUtils {
  public static int textWidth(StaticLayout layout) {
    float width = 0f;
    for (int i = 0; i < layout.getLineCount(); i++) {
      if(width < layout.getLineWidth(i)) {
        width = layout.getLineWidth(i);
      }
    }

    return (int) width;
  }


/**
 * Linearly interpolate between two values.
 */
  public static Float lerp(float a, float b, float t) {
    return a + (b -a ) * t;
  }
}
